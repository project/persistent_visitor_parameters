<?php

namespace Drupal\persistent_visitor_parameters\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\persistent_visitor_parameters\Constants\PersistentVisitorParametersConstants;

/**
 * The persistent_visitor_parameters settings form.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritDoc}
   */
  protected function getEditableConfigNames() {
    return [
      'persistent_visitor_parameters.settings',
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'persistent_visitor_parameters_settings_form';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('persistent_visitor_parameters.settings');

    $getParametersDefaultValue = implode('|', $config->get('get_parameters'));

    $form['get_parameters'] = [
      '#type' => 'textfield',
      '#title' => $this->t('List of GET parameters'),
      '#description' => $this->t('List GET parameters for tracking, separated by "|". Example: "utm_source|utm_medium|utm_campaign|utm_content|utm_term"'),
      '#default_value' => $getParametersDefaultValue,
    ];

    $serverParametersDefaultValue = implode('|', $config->get('server_parameters'));

    $form['server_parameters'] = [
      '#type' => 'textfield',
      '#title' => $this->t('List of SERVER parameters'),
      '#description' => $this->t('List of SERVER parameters for tracking, separated by "|". Example: "HTTP_REFERER|HTTP_USER_AGENT"'),
      '#default_value' => $serverParametersDefaultValue,
    ];

    $form['server_parameters_on_get_parameters_only'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Only track server parameters if at least one of the get parameters is present.'),
      '#default_value' => $config->get('server_parameters_on_get_parameters_only'),
      '#states' => [
        'visible' => [
          ':input[name="get_parameters"]' => ['!value' => ''],
          'and' => 'and',
          ':input[name="server_parameters"]' => ['!value' => ''],
        ],
        'unchecked' => [
          [':input[name="get_parameters"]' => ['value' => '']],
          [':input[name="server_parameters"]' => ['value' => '']],
        ],
      ],
    ];

    $form['mode'] = [
      '#type' => 'radios',
      '#title' => $this->t('Parameters save mode'),
      '#description' => $this->t('Consider only very first-touch parameters, or allow it to be overridden with last-touch parameters?'),
      '#options' => [
        'first_touch' => $this->t('First-touch'),
        'last_touch' => $this->t('Last-touch'),
      ],
      '#default_value' => $config->get('mode'),
    ];

    $cookieExpireDefaultValue = $config->get('cookie_expire');
    if ($cookieExpireDefaultValue != PersistentVisitorParametersConstants::COOKIE_EXPIRE_SESSION && $cookieExpireDefaultValue != PersistentVisitorParametersConstants::COOKIE_EXPIRE_NEVER) {
      $cookieExpireDefaultValue = 'custom';
    }
    $form['cookie_expire'] = [
      '#type' => 'radios',
      '#title' => $this->t('Cookie expiration'),
      '#options' => [
        PersistentVisitorParametersConstants::COOKIE_EXPIRE_SESSION => $this->t('Session'),
        PersistentVisitorParametersConstants::COOKIE_EXPIRE_NEVER => $this->t('Never'),
        'custom' => $this->t('Custom'),
      ],
      '#default_value' => $cookieExpireDefaultValue,
    ];
    $form['custom_expire'] = [
      '#type' => 'number',
      '#title' => $this->t('Custom duration'),
      '#description' => $this->t('The time the cookie expires. This is the number of seconds from the current time.'),
      '#default_value' => $config->get('cookie_expire'),
      '#states' => [
        'visible' => [
          ':input[name="cookie_expire"]' => ['value' => 'custom'],
        ],
        'required' => [
          ':input[name="cookie_expire"]' => ['value' => 'custom'],
        ],
      ],
    ];

    $form['dont_respect_dnt'] = [
      '#type' => 'checkbox',
      '#title' => $this->t("Don't respect DNT?"),
      '#description' => $this->t('You can choose to do not respect visitors browser DNT setting (Do Not Track)'),
      '#default_value' => $config->get('dont_respect_dnt'),
    ];

    $form['allow_unsafe_deserialization'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow unsafe deserialization [DEPRECATED]'),
      '#description' => $this->t('Cookie content used to be serialized instead of JSON encoded. Enable it, to allow unserializing cookie content if it is not valid json. Be careful with this option, as it may lead to security vulnerabilities. Support for serialization will be removed in the next major release (2.x).'),
      '#default_value' => $config->get('allow_unsafe_deserialization'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $cookieExpireValue = $form_state->getValue('cookie_expire');
    if ($cookieExpireValue == 'custom') {
      $cookieExpireValue = $form_state->getValue('custom_expire');
    }

    $this->config('persistent_visitor_parameters.settings')
      ->set('get_parameters', !empty($form_state->getValue('get_parameters')) ? array_filter(array_map('trim', explode('|', $form_state->getValue('get_parameters')))) : [])
      ->set('server_parameters', !empty($form_state->getValue('server_parameters')) ? array_filter(array_map('trim', explode('|', $form_state->getValue('server_parameters')))) : [])
      ->set('mode', $form_state->getValue('mode'))
      ->set('cookie_expire', $cookieExpireValue)
      ->set('dont_respect_dnt', $form_state->getValue('dont_respect_dnt'))
      ->set('server_parameters_on_get_parameters_only', $form_state->getValue('server_parameters_on_get_parameters_only'))
      ->set('allow_unsafe_deserialization', $form_state->getValue('allow_unsafe_deserialization'))
      ->save();
  }

}
