<?php

namespace Drupal\persistent_visitor_parameters;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\persistent_visitor_parameters\Constants\PersistentVisitorParametersConstants;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * The cookie manager service.
 */
class CookieManager {

  /**
   * The name of the cookie used to store defined query parameters.
   */
  const COOKIE_NAME = 'pvp_stored_variables';

  /**
   * The configuration object.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $config;

  /**
   * Constructs a CookieManager object.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   The request stack.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The configuration factory.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   */
  public function __construct(
    protected RequestStack $requestStack,
    ConfigFactoryInterface $configFactory,
    protected TimeInterface $time,
  ) {
    $this->config = $configFactory->get('persistent_visitor_parameters.settings');
  }

  /**
   * Sets a cookie with the necessary parameters.
   *
   * @param \Symfony\Component\HttpFoundation\Response $response
   *   The response object.
   */
  public function setCookie($response) {
    $serverParams = $this->neededServerParams();
    $getParams = $this->neededGetParams();

    // If we are tracking with GET parameters only, and there are no GET
    // parameters, we can return early:
    if ($this->config->get('server_parameters_on_get_parameters_only') && empty($getParams)) {
      return;
    }

    $cookieContent = $getParams + $serverParams;

    // If expected parameters are missing, we don't need to do anything.
    if (empty($cookieContent)) {
      return;
    }

    // Get the Parameter save mode.
    $mode = $this->config->get('mode');

    // If first-touch mode activated, and cookie already exists... skip the new
    // one. Otherwise - replace it with a new one:
    if ($mode === 'first_touch' && !empty($this->getCookie())) {
      return;
    }

    $cookieExpire = $this->config->get('cookie_expire');

    // If we are using custom cookie expiration, we need to add the current time
    // to the expiration time, when setting the cookie:
    if ($cookieExpire != PersistentVisitorParametersConstants::COOKIE_EXPIRE_SESSION && $cookieExpire != PersistentVisitorParametersConstants::COOKIE_EXPIRE_NEVER) {
      $cookieExpire += $this->time->getRequestTime();
    }

    $cookie = Cookie::create(self::COOKIE_NAME, Json::encode($cookieContent), $this->config->get('cookie_expire'));
    $response->headers->setCookie($cookie);
  }

  /**
   * Retrieves the cookie content.
   *
   * @return array|null
   *   The decoded cookie content or NULL if the cookie does not exist.
   */
  public function getCookie() {
    $cookie = $this->requestStack->getCurrentRequest()->cookies->get(self::COOKIE_NAME);
    if (empty($cookie)) {
      return NULL;
    }
    $decodedCookie = Json::decode($cookie);
    // @todo Remove this in next major release, no deprecation notice needed
    // here, as it would spam the logs.
    // If the decoding failed, $decodedCookie will be NULL or FALSE, so we try
    // to unserialize the cookie content if the configuration allows it:
    if ($this->config->get('allow_unsafe_deserialization') && empty($decodedCookie)) {
      // @codingStandardsIgnoreStart
      $decodedCookie = unserialize($cookie);
      // @codingStandardsIgnoreEnd
    }
    return $decodedCookie;
  }

  /**
   * Gets the necessary GET parameters from the request.
   *
   * @return array
   *   An array of necessary GET parameters.
   */
  public function neededGetParams() {
    $queryParams = $this->requestStack->getCurrentRequest()->query->all();
    return array_intersect_key($queryParams, array_flip($this->config->get('get_parameters')));
  }

  /**
   * Gets the necessary server parameters from the request.
   *
   * @return array
   *   An array of necessary server parameters.
   */
  public function neededServerParams() {
    $serverParams = $this->requestStack->getCurrentRequest()->server->all();
    return array_intersect_key($serverParams, array_flip($this->config->get('server_parameters')));
  }

  /**
   * Checks if the "Do Not Track" (DNT) header should be respected.
   *
   * @return bool
   *   TRUE if DNT should not be respected, FALSE otherwise.
   */
  public function dontRespectDnt() {
    return $this->config->get('dont_respect_dnt');
  }

}
