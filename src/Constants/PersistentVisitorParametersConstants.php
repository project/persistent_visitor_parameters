<?php

namespace Drupal\persistent_visitor_parameters\Constants;

/**
 * Constants for the Persistent Visitor Parameters module.
 */
class PersistentVisitorParametersConstants {

  const COOKIE_EXPIRE_SESSION = 0;
  const COOKIE_EXPIRE_NEVER = 2147483647;

}
