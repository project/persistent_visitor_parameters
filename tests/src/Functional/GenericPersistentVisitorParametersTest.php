<?php

namespace Drupal\Tests\persistent_visitor_parameters\Functional;

use Drupal\Tests\system\Functional\Module\GenericModuleTestBase;

/**
 * Generic module test for persistent_visitor_parameters.
 *
 * @group persistent_visitor_parameters
 */
class GenericPersistentVisitorParametersTest extends GenericModuleTestBase {

  /**
   * {@inheritDoc}
   */
  protected function assertHookHelp(string $module): void {
    // Don't do anything here. Just overwrite this useless method, so we do
    // don't have to implement hook_help().
  }

}
