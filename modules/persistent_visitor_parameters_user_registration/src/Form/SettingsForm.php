<?php

declare(strict_types=1);

namespace Drupal\persistent_visitor_parameters_user_registration\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Configure Persistent visitor parameters user registration settings.
 */
final class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'persistent_visitor_parameters_user_registration_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['persistent_visitor_parameters_user_registration.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config('persistent_visitor_parameters_user_registration.settings');
    $form['enable_utm_user_logging'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable UTM User Logging'),
      '#default_value' => $config->get('enable_utm_user_logging'),
      '#description' => $this->t('Check this to enable logging of UTM parameters for new user registrations. ONLY the following parameters are supported: <ul><li>utm_source</li><li>utm_medium</li><li>utm_campaign</li><li>utm_content</li><li>utm_term</li><li>HTTP_REFERER</li></ul><br>Note, that you have to specify the following parameters <a href=":url" target="_blank">here</a> first:<br><strong>Get Parameters:</strong> "utm_source|utm_medium|utm_campaign|utm_content|utm_term"<br><strong>Server Parameters:</strong> "HTTP_REFERER"', [':url' => Url::fromRoute('persistent_visitor_parameters.settings')->toString()]),
    ];
    $form['track_registration_url'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Track Registration URL'),
      '#default_value' => $config->get('track_registration_url'),
      '#description' => $this->t('Check this to track the URL on which the registration form was submitted.'),
      '#states' => [
        'visible' => [
          ':input[name="enable_utm_user_logging"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['track_current_route_fallback'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Track current request parameters as fallback'),
      '#default_value' => $config->get('track_http_referrer_fallback'),
      '#description' => $this->t('When enabled, if the cookie content is empty (e.g. no cookie set), the utm parameters & "HTTP_REFERER" are being fetched from the current request.'),
      '#states' => [
        'visible' => [
          ':input[name="enable_utm_user_logging"]' => ['checked' => TRUE],
        ],
      ],
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->config('persistent_visitor_parameters_user_registration.settings')
      ->set('enable_utm_user_logging', $form_state->getValue('enable_utm_user_logging'))
      ->set('track_registration_url', $form_state->getValue('track_registration_url'))
      ->set('track_http_referrer_fallback', $form_state->getValue('track_current_route_fallback'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
